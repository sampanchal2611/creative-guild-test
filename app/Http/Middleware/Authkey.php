<?php

namespace App\Http\Middleware;

use Closure;

class Authkey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token =$request->header('APP_KEY');
        if($token != 'ABCD1234')
        {
            return  response()->json(['success' => false, 'message' => 'Auth Key Missing',],401);
        }
        return $next($request);
    }
}
