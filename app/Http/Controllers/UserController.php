<?php
namespace App\Http\Controllers;

use App\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    /**
     * particular Records otherwise Get All recordGet
     *@param  id
     * @return \Illuminate\Http\Response
     */
    public function index($id = array())
    {
           if(!empty($id))
            {      
             $user = UserModel::with(['album'])->find($id)->makeHidden(['id','email_verified_at','created_at','updated_at']); 
             $user->album->makeHidden(['user_id','created_at','updated_at']);
             foreach ($user->album as $key => $field) {
                    if ($field['featured']==1 ) {
                        $user->album[$key]['featured'] = true;
                    } else {
                        $user->album[$key]['featured'] = false;
                    }
                }
             }
            else{     
             $user = UserModel::get();          
             }
        return response()->json($user);
    }
    /**
    /** Get data in particular flag wise 
     *@param  id,name,email 
     @return \Illuminate\Http\Response
     **/
    public function search(Request $request)
    {          
        $user = UserModel::where(function($q) use ($request) {
                    $q->orWhere('id', 'like', "%{$request->flag}%");
                    $q->orWhere('name', 'like', "%{$request->flag}%");
                    $q->orWhere('email', 'like', "%{$request->flag}%");})->get();           
         if (!empty($user))   
         {
           $status = true;
           $message = 'User retrieved successfully.';
         }else{
            $status=false;  
            $message = 'Someting Went wrong..';
         }
          return response()->json(['success' => $status, 'message' => $message, 'data' => $user]);;
        
    }
    /**
    @@ Get data in particular site and file and Display in dashboard Web view.@@
     */
    public function show(UserModel $userModel)
    {    
        $result= json_decode(file_get_contents(public_path('landscapes.json')), true);
        return View("app", compact('result'));
        //
    }
    /**
    @@ save user profile via api
    *@param name,email,bio,profile_picture,password,created_at,updated_at
     */
    public function save(Request $request){
        $input = $request->all();   
        //User profie images upload
        if (isset($input['profile_picture']) && !empty($input['profile_picture'])) {
            $input['profile_picture']=$this->saveImage($input['profile_picture']);
        }else {
           $input['profile_picture']= "profile.jpeg";
        }
        $data=UserModel::create($input);
        if (!empty($input))   
         {
           $status = true;
           $message = 'User retrieved successfully.';
         }else{
            $status=false;  
            $message = 'Someting Went wrong..';
         }
        return response()->json(['success' => $status, 'message' => $message, 'data' => $data],200);;
    }
    /**
    @@ save user profile images
     * *@param $image array();
     */
    public function saveImage($images =array()){
                $file = $images->getClientOriginalName();
                $filename = pathinfo($file, PATHINFO_FILENAME);
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                $imagename= $filename .'_'.time().'_' . '.' . $extension;  
                $path= $images->move(public_path() . '/img/', $imagename);
               return $imagename;                 
    }
    
    
    
}
