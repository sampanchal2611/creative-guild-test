<?php

namespace App\Http\Controllers;

use App\AlbumModel;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AlbumModel  $albumModel
     * @return \Illuminate\Http\Response
     */
    public function show(AlbumModel $albumModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AlbumModel  $albumModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AlbumModel $albumModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AlbumModel  $albumModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AlbumModel $albumModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AlbumModel  $albumModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlbumModel $albumModel)
    {
        //
    }
}
