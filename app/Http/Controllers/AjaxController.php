<?php

namespace App\Http\Controllers;

use App\ajax;
use Illuminate\Http\Request;
use Response;
class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("ajax");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        return response()->json($request->all());
//      return Response::json($request::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ajax  $ajax
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        if($request)
    {            
             return "ths i name sam";}
           
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ajax  $ajax
     * @return \Illuminate\Http\Response
     */
    public function edit(ajax $ajax)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ajax  $ajax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ajax $ajax)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ajax  $ajax
     * @return \Illuminate\Http\Response
     */
    public function destroy(ajax $ajax)
    {
        //
    }
}
