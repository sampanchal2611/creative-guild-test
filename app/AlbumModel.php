<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumModel extends Model
{
    protected $table = 'albums';
    
     protected $fillable = [
        'title',
         'description',
         'img',
         'date',
         'featured',
         'user_id',
         'updated_by'
    ];
    //
}
