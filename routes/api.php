<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
App authentic Key
APP_KEY :: ABCD1234 
 *  */
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*When pass id get Particulare user  otherwise get all user */
route::get('/usergllary/{id?}', 'UserController@index');
/*Search user profile */
route::post('/usersearch', 'UserController@search');  
/*create user profile */
route::post('/usercreate','UserController@save');



