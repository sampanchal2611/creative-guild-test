<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
    <script src="{{ URL::asset('js/style.css') }}"></script>
    <title>Test</title>
  </head>
  <body>
    <div class="container">
      <main class="main">
        <!-- Person Info -->
        <section class="person">
          <div class="person-photo">
              <img class="profile-photo" alt="profile-photo" src="{{ replaceImage($result['profile_picture']) ??''}}" />
          </div>
          <div class="person-info">
            <h1 class="name">{{ $result['name'] ??''}}</h1>
            <h5 class="bio grey">bio</h5>
            <p class="bio-info grey">{{ $result['bio'] ??''}}</p>
          </div>
          <div class="person-contact-info">
            <h5 class="grey">Phone</h5>
            <small class="contact contact-number">{{ $result['phone'] ??''}}</small>
            <h5 class="grey">Email</h5>
            <small class="contact contact-email">{{ $result['email'] ??''}}</small>
          </div>
        </section>

        <!-- Gallery Section -->
        <section id="album" class="gallery">
            @foreach($result['album'] as $res)
            <div class="album">
                 <img src="{{ replaceImage($res['img'] ??'') }}" alt="">
                  <h3 class="album-title">{{ $res['title'] ??''   }}</h3>
                <p class="album-info">{{ $res['description'] ??''   }}</p>
                <div class="fetaure-date">
                <div class="feature-icon {{ $res['featured'] ? 'true' : 'false'}}"></div>
                <div class="date grey">{{ $res['date'] ??''   }} </div>
          </div>
          </div>

    @endforeach
          </section>
      </main>
    </div>
  </body>
</html>
<?php
function replaceImage($ext){
    return str_replace("jpg","jpeg",$ext);   
}
?>