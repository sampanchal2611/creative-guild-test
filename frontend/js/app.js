//** Call Funcation **//
document.addEventListener('DOMContentLoaded', getRemote);
//** ajax synchronous call **//
function getRemote() {
  return $.ajax({
    type: "GET",
    url: 'landscapes.json',
    async: false,
    success: function (responseText) {

      const response = jQuery.parseJSON(JSON.stringify(responseText));
      // assigning response
      var headHtml = `<div class="person-photo">
            <img class="profile-photo" alt="profile-photo" src="${replaceImage(response.profile_picture)}">
          </div>
          <div class="person-info">
            <h1 class="name">${response.name}</h1>
            <h5 class="bio grey">bio</h5>
            <p class="bio-info grey">${response.bio}</p>
          </div>
          <div class="person-contact-info">
            <h5 class="grey">Phone</h5>
            <small class="contact contact-number">${response.phone}</small>
            <h5 class="grey">Email</h5>
            <small class="contact contact-email">${response.email}</small>
          </div>`;
      document.querySelector('#heading').insertAdjacentHTML('beforeend', headHtml);
      response.album.forEach((album) => {
        const html = `
          <div class="album">
          <a href="${replaceImage(album.img)}">
            <img class="album-img" src=${replaceImage(album.img)} alt="${replaceImage(album.img)}">
          </a>
          <h3 class="album-title">${album.title}</h3>
          <p class="album-info">${album.description}</p>
          <div class="fetaure-date grey">
            <div class="feature-icon ${album.featured}"></div>
            <div class="date">${album.date} </div>
          </div>
          </div>
          `;
        document.querySelector('.gallery').insertAdjacentHTML('beforeend', html);
      });

    }
  });
}
// function for replace image string.
function replaceImage(string) {
  return string.replace('jpg', 'jpeg');
}